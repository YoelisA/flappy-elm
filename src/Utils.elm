module Utils exposing
    ( Dimension
    , Position
    , Velocity
    )


type alias Position =
    { x : Float
    , y : Float
    }


type alias Dimension =
    { width : Float, height : Float }


type alias Velocity =
    { vx : Float
    , vy : Float
    }
