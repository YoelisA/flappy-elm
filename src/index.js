import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';
import largePipeUpright from './assets/pineapple.png'
import maceo from './assets/naty-bird-alive.png'
import vodka from './assets/vodka.png'
import lemon from './assets/lemon.png'
import maceoDead from './assets/naty-bird-dead.png'
import background from './assets/background-2.png'

Elm.Main.init({
  node: document.getElementById('root'),
  flags : {largePipeUpright: largePipeUpright
    , maceo: maceo, vodka : vodka, lemon: lemon, maceoDead : maceoDead, background : background}

});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
