module Scheduler exposing (..)


type alias Schedule =
    List Window


type Window
    = Unavailable { from : Float, to : Float }
    | Available { from : Float, to : Float }


day =
    { from = 8.0, to = 18.0 }


idealDay =
    List.range (round day.from) (round day.to)

idealHour =
    List.range 0 60



mySchedule : Schedule
mySchedule =
    [ Unavailable { from = 9.0, to = 10.0 }
    , Unavailable { from = 10.3, to = 11.15 }
    , Unavailable { from = 12.3, to = 13.0 }
    ]


yourSchedule : Schedule
yourSchedule =
    [ Unavailable { from = 9.0, to = 10.0 }
    , Unavailable { from = 10.3, to = 11.15 }
    , Unavailable { from = 12.3, to = 13.0 }
    ]
