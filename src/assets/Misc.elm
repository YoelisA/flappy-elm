module Misc exposing (..)
{--    let
        flappy =
            let
                { x, y } =
                    model.flappy
            in
            { x = x
            , y = y
            , bodyHeight = 30
            , bodyWidth = 40
            , legHeight = 15
            , legWidth = 15
            , eyeHeight = 5
            , eyeWidth = 5
            }

        body =
            Svg.rect
                [ Attributes.width (Types.px flappy.bodyWidth)
                , Attributes.height (Types.px flappy.bodyHeight)
                , Attributes.x (Types.px flappy.x)
                , Attributes.y (Types.px flappy.y)
                ]
                []

        legOne =
            Svg.rect
                [ Attributes.width (Types.px flappy.legWidth)
                , Attributes.height (Types.px flappy.legHeight)
                , Attributes.x (Types.px (flappy.x - 10))
                , Attributes.y (Types.px (flappy.y + 20))
                , Attributes.fill (Types.Paint Color.white)
                ]
                []

        legTwo =
            Svg.rect
                [ Attributes.width (Types.px flappy.legWidth)
                , Attributes.height (Types.px flappy.legHeight)
                , Attributes.x (Types.px (flappy.x + 30))
                , Attributes.y (Types.px (flappy.y + 20))
                , Attributes.fill (Types.Paint Color.white)
                ]
                []

        eye =
            Svg.rect
                [ Attributes.width (Types.px flappy.eyeWidth)
                , Attributes.height (Types.px flappy.eyeHeight)
                , Attributes.x (Types.px (flappy.x + 30))
                , Attributes.y (Types.px (flappy.y + 5))
                , Attributes.fill (Types.Paint Color.white)
                ]
                []
    in
    {--Svg.g
        [ Attributes.stroke (Types.Paint Color.black) ]
        [ body
        , legOne
        , legTwo
        , eye
        ]--}--}
