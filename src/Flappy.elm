module Flappy exposing
    ( Flappy
    , Status(..)
    , create
    , dimension
    , flap
    , fromVx
    , fromVy
    , fromX
    , fromY
    , getPosition
    , getScore
    , getStatus
    , getVelocity
    , isCollide
    , newFrame
    , radius
    , render
    , static
    , updatePosition
    , updateVelocity
    )

import Obstacle
    exposing
        ( Obstacle
        , getPosition
        , obstacleHeight
        , obstacleWidth
        )
import TypedSvg as Svg exposing (svg)
import TypedSvg.Attributes as Attributes exposing (height, viewBox, width)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (Paint(..), Transform(..), pc, px)
import Utils exposing (Dimension, Position, Velocity)
import World exposing (Gravity, gravity, limit)


type Flappy
    = Flappy Radius Position Velocity Rotation Status Score


type alias Radius =
    Float


type alias Rotation =
    Float


flapHeight =
    0.5


radius =
    20


type Status
    = Dead
    | Alive


type alias Score =
    Int


getRotation : Flappy -> Rotation
getRotation (Flappy _ _ _ rotation _ _) =
    rotation


collisionDetection : Float -> Float -> Float -> Float -> Float -> Float -> Float -> Bool
collisionDetection cx cy r rx ry rw rh =
    let
        distanceX =
            abs (cx - rx - rw / 2)

        distanceY =
            abs (cy - ry - rh / 2)

        distance =
            sqrt (distanceX ^ 2 + distanceY ^ 2)

        dx =
            distanceX - rw / 2

        dy =
            distanceY - rh / 2
    in
    if distanceX > rw / 2 + r then
        False

    else if distanceY > rh / 2 + r then
        False

    else if distanceX <= rw / 2 then
        True

    else if distanceY <= rh / 2 then
        True

    else
        True


isCollide : Flappy -> Obstacle -> Flappy
isCollide (Flappy radius_ flappy_ velocity rotation status score) obstacle =
    let
        { x, y } =
            Obstacle.getPosition obstacle
    in
    case collisionDetection flappy_.x flappy_.y radius x y obstacleWidth obstacleHeight of
        True ->
            Flappy radius flappy_ velocity rotation Dead score

        False ->
            Flappy radius
                flappy_
                velocity
                rotation
                status
                (if x < flappy_.x then score + 1 else score)


dimension : Dimension
dimension =
    { width = 40, height = 40 }


create : Flappy
create =
    Flappy radius { x = 250, y = 250 } { vx = 0, vy = 0 } 0 Alive 0


type alias DeltaTime =
    Float


bounded =
    clamp World.limit.top (World.limit.bottom - radius)


boundedVelocity =
    clamp -0.5 0.5



{--

--}


newFrame : Flappy -> DeltaTime -> Flappy
newFrame prevFlappy deltaTime =
    let
        { x, y } =
            getPosition prevFlappy

        { vx, vy } =
            getVelocity prevFlappy

        rotate =
            getRotation prevFlappy

        newPosition =
            { x = x, y = bounded <| y + vy * deltaTime }

        newVelocity =
            { vx = vx, vy = boundedVelocity <| vy + gravity.vy * deltaTime }

        newRotation =
            if newVelocity.vy > vy then
                clamp -45 45 rotate - 3

            else
                clamp -45 45 rotate + 5

        status =
            getStatus prevFlappy

        score =
            getScore prevFlappy
    in
    Flappy radius newPosition newVelocity newRotation status score


static : Flappy
static =
    Flappy radius (Position 250 250) (Velocity 0 0) 0 Alive 0


updatePosition : Flappy -> Position -> Flappy
updatePosition flappy newPosition_ =
    let
        prevPosition =
            getPosition flappy

        prevVelocity =
            getVelocity flappy

        status =
            getStatus flappy

        score =
            getScore flappy
    in
    Flappy radius newPosition_ prevVelocity (getRotation flappy) status score


updateVelocity : Flappy -> Velocity -> Flappy
updateVelocity flappy newVelocity_ =
    let
        prevPosition =
            getPosition flappy

        prevVelocity =
            getVelocity flappy

        status =
            getStatus flappy

        score =
            getScore flappy
    in
    Flappy radius prevPosition newVelocity_ (getRotation flappy) status score


flap : Velocity -> Velocity
flap prevVelocity =
    let
        { vy, vx } =
            prevVelocity
    in
    { vx = vx, vy = vy - flapHeight }


getVelocity : Flappy -> Velocity
getVelocity flappy =
    case flappy of
        Flappy _ _ { vx, vy } _ _ _ ->
            { vx = vx, vy = vy }


getPosition : Flappy -> Position
getPosition flappy =
    case flappy of
        Flappy _ { x, y } _ _ _ _ ->
            { x = x, y = y }


fromX : Flappy -> Float
fromX flappy =
    let
        { x, y } =
            getPosition flappy
    in
    x


fromY : Flappy -> Float
fromY flappy =
    let
        { x, y } =
            getPosition flappy
    in
    y


fromVx : Flappy -> Float
fromVx flappy =
    let
        { vx, vy } =
            getVelocity flappy
    in
    vx


fromVy : Flappy -> Float
fromVy flappy =
    let
        { vx, vy } =
            getVelocity flappy
    in
    vy


getStatus : Flappy -> Status
getStatus flappy =
    case flappy of
        Flappy _ _ _ _ status _ ->
            status


getScore : Flappy -> Score
getScore flappy =
    case flappy of
        Flappy _ _ _ _ _ score ->
            score


type alias Assets =
    { largePipeUpright : String
    , maceo : String
    , vodka : String
    , lemon : String
    , maceoDead : String
    , background : String
    }


render : Flappy -> Assets -> Svg msg
render flappy image =
    let
        func =
            Svg.svg []
                [ Svg.defs []
                    [ Svg.pattern
                        [ Attributes.id "attachedImage"
                        , Attributes.width (pc 100)
                        , Attributes.width (pc 100)
                        ]
                        [ Svg.image
                            [ Attributes.xlinkHref image.maceoDead
                            , Attributes.width (px 60)
                            , Attributes.width (px 60)
                            ]
                            []
                        ]
                    ]
                ]

        renderWith image_ =
            Svg.svg
                []
                [ Svg.circle
                    [ Attributes.cx (px (fromX flappy))
                    , Attributes.cy (px (fromY flappy))
                    , Attributes.r (px radius)
                    , Attributes.transform
                        [ Rotate (getRotation flappy) (fromX flappy) (fromY flappy)
                        ]
                    , Attributes.fill (Reference "url(#attachedImage)")
                    ]
                    []
                ]
    in
    case getStatus flappy of
        Dead ->
            renderWith image.maceoDead

        Alive ->
            renderWith image.maceo



{--Svg.image
    [ Attributes.xlinkHref image_
    , Attributes.x (px (fromX flappy))
    , Attributes.y (px (fromY flappy))
    , Attributes.transform [ Rotate (getRotation flappy) (fromX flappy) (fromY flappy) ]
    ]
    []
,--}
