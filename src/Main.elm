module Main exposing (..)

import Browser
import Browser.Dom exposing (Viewport, getViewport, getViewportOf, setViewport, setViewportOf)
import Browser.Events exposing (onAnimationFrameDelta, onKeyDown)
import Color exposing (black, white)
import Element
    exposing
        ( centerX
        , centerY
        , clip
        , column
        , html
        , htmlAttribute
        , layout
        , px
        , scrollbarX
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Flappy
    exposing
        ( Flappy
        , Status(..)
        , create
        , dimension
        , flap
        , fromVx
        , fromVy
        , fromX
        , fromY
        , isCollide
        , newFrame
        , render
        , static
        , updatePosition
        , updateVelocity
        )
import Html exposing (Html)
import Html.Attributes exposing (id, src)
import Json.Decode as Decode
import Obstacle
    exposing
        ( Obstacle
        , PosFromPoint(..)
        , Status(..)
        , Type_(..)
        , cleanGarbage
        , create
          --, generateObstacle
        , generateObstacle
        , getPosition
        , newFrame
        , newScore
        , newStatus
        , renderObstacle
        )
import Palette exposing (paletteColor, paletteElement)
import Process exposing (sleep)
import Random exposing (..)
import Task exposing (..)
import TypedSvg as Svg
import TypedSvg.Attributes as Attributes
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types as Types exposing (CoordinateSystem(..), px)
import Utils exposing (Dimension, Position, Velocity)
import World
    exposing
        ( Gravity
        , dimension
        , gravity
        , limit
        , render
        )



---- MODEL ----
-- obstacle : [(Vodka, Vodka), (Ananas, Ananas)]


type alias Model =
    { flappy : Flappy
    , obstacles : List Obstacle
    , game : GameStatus
    , assets : Assets
    , score : Int
    , viewport : Maybe Viewport
    , background : Float
    , fps : Float
    , counter : Int
    , spawnAt : Int
    }


type GameStatus
    = Paused
    | Playing
    | GameOver
    | Intro


init : Assets -> ( Model, Cmd Msg )
init assets =
    ( { flappy = Flappy.create
      , obstacles = []
      , game = Intro
      , assets = assets
      , score = 0
      , background = -400
      , viewport = Nothing
      , fps = 0
      , counter = 0
      , spawnAt = 300
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | OnTick Float
    | KeyPressed Action
    | GotObstacle { type_ : Type_, yPosition : Float, spawnFrequencyVariation : Float }
    | SpawnObstacle


initGame assets =
    { flappy = Flappy.create
    , obstacles =
        []
    , game = Intro
    , assets = assets
    , score = 0
    , viewport = Nothing
    , background = -400
    , fps = 0
    , counter = 0
    , spawnAt = 100000
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        OnTick dt ->
            let
                delta =
                    clamp 15 18 dt

                obstacles =
                    model.obstacles

                ceilingCollision =
                    fromY model.flappy <= World.limit.top + Flappy.radius

                floorCollision =
                    fromY model.flappy >= World.limit.bottom - Flappy.radius

                collision =
                    obstacles
                        |> List.map (\obstacle -> Flappy.isCollide model.flappy obstacle)

                shouldBeDead =
                    collision
                        |> List.foldl
                            (\flappy false ->
                                if Flappy.getStatus flappy == Dead then
                                    not false

                                else
                                    false
                            )
                            False

                newFlappy =
                    case model.game of
                        Playing ->
                            Flappy.newFrame model.flappy delta

                        _ ->
                            Flappy.static
            in
            ( { model
                | flappy =
                    newFlappy
                , obstacles =
                    if model.game == Intro || model.game == GameOver then
                        model.obstacles

                    else
                        Obstacle.newFrame
                            model.obstacles
                            (Flappy.getPosition model.flappy)
                            delta
                , game =
                    if shouldBeDead || ceilingCollision || floorCollision then
                        GameOver

                    else if model.game /= Intro then
                        Playing

                    else
                        Intro
                , fps = delta
                , score =
                    List.foldr
                        (\a acc ->
                            if Obstacle.getStatus a == Dodged then
                                acc + 1

                            else
                                acc
                        )
                        0
                        model.obstacles
                , background =
                    if model.background < -690 then
                        -400

                    else
                        model.background - 0.04 * delta
                , counter =
                    if model.counter > 300 then
                        model.counter - 300

                    else
                        model.counter + 1
              }
            , if model.counter == 300 then
                Process.sleep 10 |> Task.perform (\_ -> SpawnObstacle)

              else
                Cmd.none
            )

        KeyPressed playerAction ->
            case model.game of
                Intro ->
                    case playerAction of
                        StartGamePressed ->
                            ( { model | game = Playing, obstacles = [], counter = 0 }
                            , Process.sleep 50
                                |> Task.perform
                                    (\_ ->
                                        SpawnObstacle
                                    )
                            )

                        _ ->
                            ( model, Cmd.none )

                Playing ->
                    case playerAction of
                        SpaceBarPressed ->
                            ( { model
                                | flappy =
                                    Flappy.updateVelocity
                                        model.flappy
                                        (Flappy.flap (Flappy.getVelocity model.flappy))
                                , obstacles = model.obstacles |> cleanGarbage
                              }
                            , Cmd.none
                            )

                        PauseKeyPressed ->
                            ( { model
                                | game =
                                    Paused
                              }
                            , Cmd.none
                            )

                        _ ->
                            ( model
                            , Cmd.none
                            )

                GameOver ->
                    case playerAction of
                        RestartPressed ->
                            ( initGame model.assets, Cmd.none )

                        _ ->
                            ( initGame model.assets, Cmd.none )

                Paused ->
                    case playerAction of
                        PauseKeyPressed ->
                            ( { model | game = Playing }, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

        GotObstacle newObstacle ->
            case model.game of
                Playing ->
                    ( { model
                        | obstacles =
                            Obstacle.create newObstacle Bottom
                                :: Obstacle.create newObstacle Top
                                :: model.obstacles
                        , spawnAt = round newObstacle.spawnFrequencyVariation
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        SpawnObstacle ->
            case model.game of
                Playing ->
                    ( model
                    , Process.sleep 100
                        |> (\_ ->
                                if model.game == Playing then
                                    generateObstacle GotObstacle

                                else
                                    Cmd.none
                           )
                    )

                _ ->
                    ( initGame model.assets, Cmd.none )


sleepAndNewObstacle variableTime =
    Process.sleep variableTime |> Task.perform (\_ -> SpawnObstacle)



---- VIEW ----


type alias Assets =
    { largePipeUpright : String
    , maceo : String
    , vodka : String
    , lemon : String
    , maceoDead : String
    , background : String
    }


viewBackground : Model -> Svg msg
viewBackground model =
    let
        background =
            model.background
    in
    Svg.image
        [ Attributes.xlinkHref model.assets.background
        , Attributes.x (Types.px background)
        , Attributes.width (Types.px 1500)
        , Attributes.height (Types.px 500)
        ]
        []


viewFlappy : Model -> Svg msg
viewFlappy model =
    let
        flappy =
            model.flappy

        assets =
            model.assets
    in
    Flappy.render model.flappy assets


viewObstacles : Model -> Svg msg
viewObstacles model =
    model.obstacles
        |> List.map
            (\obstacle ->
                renderObstacle obstacle model.assets
            )
        |> Svg.g []


view : Model -> Html Msg
view model =
    layout
        [ Background.color paletteElement.primary
        ]
    <|
        column
            [ Element.width (Element.px <| round World.dimension.width)
            , Element.height (Element.px <| round World.dimension.height)
            , centerX
            , centerY
            , Element.inFront
                (Element.el
                    [ Font.size 56
                    , Font.bold
                    , centerX
                    , Font.color Palette.paletteElement.secondary
                    ]
                    (Element.text (String.fromInt model.score))
                )
            , Element.inFront (viewGameOver model)
            ]
            [ World.render [ viewBackground model, viewFlappy model, viewObstacles model ]
                |> Element.html
            ]


viewGameOver : Model -> Element.Element Msg
viewGameOver model =
    case model.game of
        Paused ->
            column
                [ Element.width (Element.px <| round World.dimension.width)
                , Element.height (Element.px <| round World.dimension.height)
                , Background.color paletteElement.secondary
                , Element.alpha 0.8
                ]
                [ Element.el [ centerX, centerY, Font.size 56 ] (Element.text "Appuie sur 'p' pour reprendre le jeu") ]

        Playing ->
            Element.none

        GameOver ->
            column
                [ Element.width (Element.px <| round World.dimension.width)
                , Element.height (Element.px <| round World.dimension.height)
                , Background.color paletteElement.secondary
                , Element.alpha 0.8
                , Element.spacing 30
                ]
                [ Element.el [ centerX, centerY, Font.size 56 ] (Element.text "Game Over")
                , Input.button [ centerX, centerY ]
                    { label =
                        Element.el
                            [ Border.width 3
                            , Element.padding 10
                            , Element.mouseOver
                                [ Background.color paletteElement.primary
                                , Font.color paletteElement.secondary
                                ]
                            ]
                            (Element.text "Rejouer")
                    , onPress = Just (KeyPressed RestartPressed)
                    }
                ]

        Intro ->
            column
                [ Element.width (Element.px <| round World.dimension.width)
                , Element.height (Element.px <| round World.dimension.height)
                , Background.color paletteElement.secondary
                , Element.alpha 0.8
                , Element.spacing 30
                ]
                [ Element.el [ centerX, centerY, Font.size 56 ] (Element.text "Naty Bird")
                , Input.button
                    [ centerX
                    , centerY
                    ]
                    { label =
                        Element.el
                            [ Border.width 3
                            , Element.padding 10
                            , Element.mouseOver
                                [ Background.color paletteElement.primary
                                , Font.color paletteElement.secondary
                                ]
                            ]
                            (Element.text "New Game")
                    , onPress = Just (KeyPressed StartGamePressed)
                    }
                ]



---- PROGRAM ----


main : Program Assets Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }



--- Subscriptions ---


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.game of
        Paused ->
            Sub.batch [ onKeyDown (Decode.map KeyPressed decoder) ]

        Playing ->
            Sub.batch
                [ onAnimationFrameDelta OnTick
                , onKeyDown (Decode.map KeyPressed decoder)
                ]

        GameOver ->
            Sub.batch [ onKeyDown (Decode.map KeyPressed decoder) ]

        Intro ->
            Sub.batch
                [ onKeyDown (Decode.map KeyPressed decoder)
                , onAnimationFrameDelta OnTick
                ]


decoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyToAction


keyToAction : String -> Decode.Decoder Action
keyToAction key =
    case key of
        " " ->
            Decode.succeed SpaceBarPressed

        "p" ->
            Decode.succeed PauseKeyPressed

        "r" ->
            Decode.succeed RestartPressed

        "Enter" ->
            Decode.succeed StartGamePressed

        _ ->
            Decode.fail "Not an evant we care about"


type Action
    = SpaceBarPressed
    | PauseKeyPressed
    | RestartPressed
    | StartGamePressed



--noisySinusoid amplitude verticalShift horizontalShift noise x =
--  let
--    genNoise =
--      Random.generate NewInt (Random.float 10.0 x)
--in
--amplitude * sin (x - horizontalShift) + verticalShift * noise
--noisyY noise x =
--noisySinusoid 200 200 (pi / 2) noise x
--createNewObstacle : Model -> Cmd Msg
--createNewObstacle model =
--  case model.game of
--    Playing ->
--      Process.sleep 3000 |> Task.perform (\_ -> NewObstacle)
--GameOver ->
--  Cmd.none
--Paused ->
--  Cmd.none
--Start ->
--  Cmd.none
