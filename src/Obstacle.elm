module Obstacle exposing
    ( Obstacle
    , PosFromPoint(..)
    , Status(..)
    , Type_(..)
    , cleanGarbage
    , create
    , generateObstacle
    , getPosition
    , moveLeft
    , newFrame
    , newScore
    , newStatus
    , obstacleHeight
    , obstacleWidth
    , renderObstacle
    , getStatus
    )

import Animator exposing (Animator, Timeline, init)
import Color exposing (black)
import Random exposing (..)
import TypedSvg as Svg exposing (svg)
import TypedSvg.Attributes as Attributes exposing (height, viewBox, width)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (Paint(..), Transform(..), px)
import Utils exposing (Dimension, Position, Velocity)
import World exposing (dimension)



--Constants


obstacleWidth =
    70


obstacleHeight =
    500



--Main Types


type Obstacle
    = Obstacle Type_ Position Velocity Status


type Direction
    = Upright
    | UpsideDown


type Type_
    = Vodka
    | Lemon
    | PineApple


type Status
    = Out
    | In
    | Dodged


type alias GenObstaclePoint =
    { type_ : Type_
    , yPosition : Float
    , spawnFrequencyVariation : Float
    }


obstacle_ =
    Random.map3 GenObstaclePoint
        (Random.uniform Vodka [ Lemon, PineApple ])
        (Random.float -500 -120)
        (Random.float 200 250)


generateObstacle msg =
    generate msg obstacle_


type PosFromPoint
    = Top
    | Bottom


create : GenObstaclePoint -> PosFromPoint -> Obstacle
create { yPosition, type_ } posFromPoint =
    Obstacle
        type_
        (Position 750
            (case posFromPoint of
                Top ->
                    yPosition

                _ ->
                    yPosition + obstacleHeight + 140
            )
        )
        (Velocity -0.08 0)
        In


getPosition : Obstacle -> Position
getPosition (Obstacle type_ position velocity status) =
    position


getStatus : Obstacle -> Status
getStatus (Obstacle type_ position velocity status) =
    status


renderObstacle : Obstacle -> Assets -> Svg msg
renderObstacle (Obstacle type_ position velocity status) asset =
    let
        upperRectangle =
            Svg.rect [ Attributes.width (px obstacleWidth), Attributes.height (px obstacleHeight) ] []

        lowerRectangle =
            Svg.rect
                [ Attributes.width (px obstacleWidth)
                , Attributes.height (px obstacleHeight)
                , Attributes.y (px position.y)
                , Attributes.x (px position.x)
                ]
                []
    in
    Svg.svg
        []
        [ lowerRectangle
        ]


moveLeft : Obstacle -> Float -> Obstacle
moveLeft (Obstacle type_ position velocity status) delta =
    Obstacle
        type_
        { x = position.x + velocity.vx * delta, y = position.y }
        { vx = velocity.vx, vy = velocity.vy }
        status


newStatus : Obstacle -> Position -> Obstacle
newStatus (Obstacle type_ position velocity status) flappy =
    let
        { x, y } =
            flappy

        updateStatus =
            if position.x < x then
                Dodged

            else if position.x < (World.limit.left - obstacleWidth) then
                Out

            else
                In
    in
    Obstacle
        type_
        position
        velocity
        updateStatus


newScore : List Obstacle -> Int
newScore obstacles =
    obstacles
        |> List.foldl
            (\(Obstacle type_ position velocity status) acc ->
                case status of
                    Dodged ->
                        acc + 1

                    _ ->
                        acc
            )
            0


newFrame : List Obstacle -> Position -> Float -> List Obstacle
newFrame obstacles flappyPos delta =
    let
        move =
            List.map (\obstacle -> moveLeft obstacle delta)

        updateStatus =
            List.map (\obstacle -> newStatus obstacle flappyPos)

        moveThenUpdate =
            updateStatus << move
    in
    obstacles
        |> moveThenUpdate


cleanGarbage =
    List.foldr
        (\((Obstacle type_ position velocity status) as obstacle) acc ->
            case status of
                In ->
                    obstacle :: acc

                Dodged ->
                    obstacle :: acc

                Out ->
                    acc
        )
        []


type alias Assets =
    { largePipeUpright : String
    , maceo : String
    , vodka : String
    , lemon : String
    , maceoDead : String
    , background : String
    }



{--Svg.image
    [ Attributes.xlinkHref src
    , Attributes.width (px width)
    , Attributes.height (px height)
    ]
    []--}
