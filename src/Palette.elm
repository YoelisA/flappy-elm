module Palette exposing (paletteColor, paletteElement)

import Color exposing (rgb255)
import Element exposing (rgb255)


primary =
    ( 51, 73, 68 )


secondary =
    ( 255, 231, 233 )


paletteElement : PaletteElement
paletteElement =
    let
        ( r, g, b ) =
            primary

        ( r2, g2, b2 ) =
            secondary
    in
    { primary = Element.rgb255 r g b
    , secondary = Element.rgb255 r2 g2 b2
    }


paletteColor : PaletteColor
paletteColor =
    let
        ( r, g, b ) =
            primary

        ( r2, g2, b2 ) =
            secondary
    in
    { primary = Color.rgb255 r g b
    , secondary = Color.rgb255 r2 g2 b2
    , white = Color.rgb255 255 255 255
    }


type alias PaletteElement =
    { primary : Element.Color
    , secondary : Element.Color
    }


type alias PaletteColor =
    { primary : Color.Color
    , secondary : Color.Color
    , white : Color.Color
    }
