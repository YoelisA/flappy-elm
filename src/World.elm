module World exposing
    ( Gravity
    , dimension
    , gravity
    , limit
    , render
    )

import TypedSvg as Svg exposing (svg)
import TypedSvg.Attributes as Attributes exposing (height, viewBox, width)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (px)
import Utils exposing (Dimension)


type alias Gravity =
    { vx : Float
    , vy : Float
    }


type World
    = World Dimension


gravity : Gravity
gravity =
    { vx = 0.0
    , vy = 0.002
    }


limit =
    { left = 0
    , right = dimension.width
    , top = 0
    , bottom = dimension.height
    }


dimension : Dimension
dimension =
    { width = 500, height = 500 }


render : List (Svg msg) -> Svg msg
render elements =
    Svg.svg
        [ Attributes.width (px <| dimension.width)
        , Attributes.height (px dimension.height)
        , Attributes.viewBox 0 0 dimension.width dimension.height
        ]
        elements
